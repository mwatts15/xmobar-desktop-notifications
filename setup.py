from setuptools import setup
setup(name='xmonoti',
    install_requires=[
        'zodb>=4.1.0',
        'BTrees>=4.0.8',
        'zc.lockfile',
        'PyGObject',
        'persistent>=4.0.8',
        'transaction>=1.4.4',
    ],
    version='0.0.1',
    packages=['xmonoti'],
    entry_points={
        'console_scripts': ['xmonoti = xmonoti:main', 'xmonoti-cli = xmonoti:ctl'],
    },
)
