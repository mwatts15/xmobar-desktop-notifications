import logging
import os
import os.path as PTH
from collections import namedtuple
import argparse
import time

import ZODB
from ZODB.FileStorage import FileStorage
import ZODB.serialize
import transaction
from zc.lockfile import LockError
from persistent.list import PersistentList
from persistent.mapping import PersistentMapping
from gi.repository import Gio, GLib


L = logging.getLogger(__name__)


# Thanks to  Gio test files at GNOME repo: glib/blob/master/gio/tests/gdbus-example-server.c
# Generating interface from XML is easy. Look at the above file on how it is done.
# A better resource : http://maemo.org/maemo_training_material/maemo4.x/html/
#                     maemo_Platform_Development_Chinook/
#                     Chapter_03_Using_the_GLib_wrappers_for_DBus.html
#                     #DBusinterfacedefinitionusingXML


loop = GLib.MainLoop() # A loop to handle API


NotificationData = namedtuple('NotificationData',
        ('id', 'app_name', 'summary', 'body', 'actions', 'hints', 'expire_timeout'))

XDG_RUNTIME_DIR = os.environ.get('XDG_RUNTIME_DIR')


class NotificationsDB:
    def __init__(self, notifications_file, open_params=None):
        try:
            fs = FileStorage(notifications_file, **(open_params or dict()))
        except IOError:
            L.exception("Failed to create a FileStorage")
            raise FileStorageInitFailed(notifications_file)
        except LockError:
            L.exception('Found database "{}" is locked when trying to open it. '
                    'The PID of this process: {}'.format(notifications_file, os.getpid()),
                    exc_info=True)
            raise FileLocked('Database ' + notifications_file + ' locked')
        self._fs = fs
        self._zdb = ZODB.DB(fs, cache_size=1600)
        self._conn = self._zdb.open()
        self._root = self._conn.root()
        with transaction.manager:
            if 'notifications' not in self._root:
                self._init_ds()
                self._root['notifications'] = (self._notifications_map, self._notifications)
            else:
                self._notifications_map, self._notifications = self._root['notifications']

            if 'notification_count' not in self._root:
                self._root['notification_count'] = 0

    def _init_ds(self):
        self._notifications_map = PersistentMapping()
        self._notifications = PersistentList()

    @property
    def notifications(self):
        return self._notifications

    @property
    def notifications_map(self):
        return self._notifications_map

    @property
    def notification_count(self):
        return self._root['notification_count']

    @notification_count.setter
    def notification_count(self, cnt):
        self._root['notification_count'] = cnt

    def clear(self):
        with transaction.manager:
            self._notifications_map.clear()
            self._notifications.clear()
            self._fs.pack(time.time(), ZODB.serialize.referencesf)

    def close(self):
        try:
            transaction.commit()
        except Exception:
            # catch commit exception and close db.
            # otherwise db would stay open and follow up tests
            # will detect the db in error state
            L.warning('Forced to abort transaction on ZODB store closing', exc_info=True)
            transaction.abort()
        self._conn.close()
        self._zdb.close()

        self._conn = None
        self._zdb = None


class Notifications:
    def __init__(self, notifications_db, notifications_pipe_file,
                 open_params=None,
                 max_sleep_ms=1000,
                 default_timeout_ms=1000 * 60 * 60):
        self._default_timeout_ms = default_timeout_ms
        self.db = notifications_db

        try:
            os.mkfifo(notifications_pipe_file, mode=0o600)
        except FileExistsError:
            pass

        self.active_color = '#268bd2'

        self._output = open(notifications_pipe_file, 'a')

        self._max_sleep_ms = max_sleep_ms
        self._refresh_source_id = GLib.timeout_add(max_sleep_ms, self.refresh)

    @property
    def notifications(self):
        return self.db.notifications

    @property
    def notifications_map(self):
        return self.db.notifications_map

    @property
    def _notification_count(self):
        return self.db.notification_count

    @_notification_count.setter
    def _notification_count(self, cnt):
        self.db.notification_count = cnt

    def GetServerInformation(self):
        return GLib.Variant('(ssss)', ('xmobar-notifications', 'Mark Watts', '0.0.1', '1.2'))

    def GetCapabilities(self):
        return GLib.Variant('(as)', (['body', 'body-markup', 'actions', 'persistence'],))

    def CloseNotification(self, notification_id):
        self._close_notification(notification_id)
        self.refresh()

    def Notify(self,
            app_name,     # STRING
            replaces_id,  # UINT32
            app_icon,     # STRING
            summary,      # STRING
            body,         # STRING
            actions,      # as
            hints,        # a{sv}
            expire_timeout): # INT32
        with transaction.manager:
            res = replaces_id

            if not replaces_id:
                self._notification_count += 1
                res = self._notification_count
            else:
                for idx, nd0 in enumerate(self.notifications):
                    if nd0.id == replaces_id:
                        self.notifications.pop(idx)
                        break
                else: # no break
                    L.warning('Could not find the notification to be replaced')

            if expire_timeout < 0:
                expire_timeout = self._default_timeout_ms

            # We can't show images anyway, so just drop the data (it's huuuge)
            hints.pop('image-data', None)
            hints.pop('image-path', None)
            hints.pop('icon_data', None)

            nd = NotificationData(res, app_name, summary, body, actions, hints, expire_timeout)
            self.notifications.append(nd)
            self.notifications_map[res] = nd
            self.refresh()

            return GLib.Variant('(u)', (res,))

    def refresh(self):
        if self._refresh_source_id:
            refresh_id = self._refresh_source_id
            self._refresh_source_id = None
            GLib.source_remove(refresh_id)
        self._refresh_source_id = GLib.timeout_add(self._max_sleep_ms, self.refresh)
        self.print()

    def print(self):
        try:
            notif = self.notifications[-1]
            raw_summary = notif.summary[:50]
            print(f'<action=`xmonoti-cli close {notif.id}` button=2><action=`xmonoti-cli invoke {notif.id}`>',
                end='', file=self._output)
            summary = f' <fc={self.active_color}>/</fc> '.join(
                    f'<fc={self.active_color}><raw={len(raw_summary)}:{raw_summary}/></fc>'.split('\n'))
            print(summary, end='', file=self._output)
            if notif.body:
                body_parts = (f'<raw={len(part)}:{part}/>' for part in notif.body[:80].split('\n'))
                body = f' <fc={self.active_color}>/</fc> '.join(body_parts)
                print(f': {body}', end='', file=self._output)
            print('</action></action>', end='', file=self._output)
            print(f'<fc={self.active_color}>::</fc>', file=self._output)
            self._output.flush()
        except IndexError:
            L.debug('No notifications to print')
            print('', file=self._output)
            self._output.flush()
        except BrokenPipeError:
            L.warning('Unable to report notification %s', notif)

    def close(self):
        self._output.close()

    def _close_notification(self, notification_id):
        print('Closing', notification_id)
        with transaction.manager:
            for idx, nd0 in enumerate(self.notifications):
                if nd0.id == notification_id:
                    self.notifications.pop(idx)
                    del self.notifications_map[notification_id]
                    break
            else: # no break
                L.warning('Could not find the notification to be closed')


class Command:
    def __init__(self, dbus_conn, notifications_db):
        self.dbus_conn = dbus_conn
        self.notifications_db = notifications_db

    def Invoke(self, id, action):
        # It's not clear from the spec whether the signal is meant to be sent only to the
        # sender of the original notification or if it's OK to "broadcast", so we'll just
        # broadcast. Kind-of makes sense because maybe the application died or something
        # in the mean-time and we wouldn't know how to call back to it otherwise.
        self.dbus_conn.emit_signal(None,
                '/org/freedesktop/Notifications', 'org.freedesktop.Notifications',
                'ActionInvoked', GLib.Variant('(us)', (id, action)))

    def Clear(self):
        self.notifications_db.clear()


class OpenError(Exception):
    pass


class FileStorageInitFailed(OpenError):
    pass


class FileLocked(OpenError):
    pass


class MethodCallHandler:
    def __init__(self, target):
        self.target = target

    def __call__(self, connection, sender, object_path, interface_name, method_name, params, invocation):
        """
        This is the top-level function that handles all the method calls to our server.
        The first four parameters are self-explanatory.
        `method_name` is a string that describes our method name.
        `params` is a GLib.Variant that are inputs/parameters to the method.
        `invocation` is a Gio.DBusMethodInvocation, something like a messenger that transports
        our reply back to sender.
        """

        try:
            method = getattr(self.target, method_name)
        except AttributeError:
            L.warning('Unexpected attribute "%s" requested on %s', method_name, self.target)
            # Apparently D-Bus is supposed to eliminate
            invocation.return_value(None)
            return

        try:
            invocation.return_value(method(*params))
        except Exception:
            L.warning("Failure in method invocation", exc_info=True)
            invocation.return_value(None)
        # NOTE TO SELF: use .unpack() to get a Pythonic value from a variant


def on_name_acquired(connection, name):

    """
    What to do after name acquired?
    """

    print("Name acquired :", name)


def on_name_lost(connection, name):

    """
    What to do after our name is lost? May be just exit.
    """

    print("Name lost :", name)
    exit(0)


class BusAcquisitionHandler:
    def __init__(self, notif_data_file, notif_pipe_file):
        self.notifications_db = NotificationsDB(
                notif_data_file,
                open_params=dict(pack_keep_old=False))
        self.notif_pipe_file = notif_pipe_file
        self._closeables = []

    def close(self):
        for c in self._closeables:
            c.close()
        self.notifications_db.close()

    def __call__(self, connection, name):
        """
        The function that introduces our server to the world. It is called automatically
        when we get the bus we asked.
        """

        # Remember the node we made earlier? That has a list as interfaces attribute.
        # From that get our interface. We made only one interface, so we get the first
        # interface.

        print("Bus acquired for name,", name)
        node = Gio.DBusNodeInfo.new_for_xml(NOTIFICATIONS_XML)  # We make a node for the xml
        if name == 'org.freedesktop.Notifications':
            notihandler = Notifications(self.notifications_db, self.notif_pipe_file)
            self._closeables.append(notihandler)
            handler = MethodCallHandler(notihandler)
            connection.register_object(
                "/org/freedesktop/Notifications", node.interfaces[0], handler, None, None)
        else:
            handler = MethodCallHandler(Command(connection, self.notifications_db))
            connection.register_object(
                "/cc/markw/Xmonoti", node.interfaces[1], handler, None, None)


def do_list(ns):
    fname = get_notifications_fname()

    if not os.path.exists(fname):
        return

    db = NotificationsDB(fname, open_params=dict(read_only=True))
    for noti in db.notifications:
        print(noti.id, ' / '.join(noti.summary.strip().split('\n')), ':', ' / '.join(noti.body.strip().split('\n')))
        print(noti.actions)
        print(noti.hints)
        print()


def do_invoke(ns):
    xmonoti = make_xmonoti_client()
    xmonoti.call_sync('Invoke', GLib.Variant('(us)', (ns.id, ns.action)),
            Gio.DBusCallFlags.NONE, 500, None)


def do_clear(ns):
    xmonoti = make_xmonoti_client()
    xmonoti.call_sync('Clear', GLib.Variant('()', ()),
            Gio.DBusCallFlags.NONE, 500, None)


def do_close(ns):
    notifications = make_notifications_client()
    notifications.call_sync('CloseNotification', GLib.Variant('(u)', (ns.id,)),
            Gio.DBusCallFlags.NONE, 500, None)


def make_notifications_client():
    bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)

    return Gio.DBusProxy.new_sync(bus,
            Gio.DBusProxyFlags.NONE,
            None,
            'org.freedesktop.Notifications',
            '/org/freedesktop/Notifications',
            'org.freedesktop.Notifications',
            None)


def make_xmonoti_client():
    bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)

    return Gio.DBusProxy.new_sync(bus,
            Gio.DBusProxyFlags.NONE,
            None,
            'cc.markw.Xmonoti',
            '/cc/markw/Xmonoti',
            'cc.markw.Xmonoti',
            None)


def ctl():
    parser = argparse.ArgumentParser()
    sp = parser.add_subparsers(dest='command')
    sp.add_parser('list', help='List the notifications')

    close = sp.add_parser('close', help='Close a notification')
    close.add_argument('id', type=int, help='Notification ID')

    invoke = sp.add_parser('invoke', help='Invoke an action on a notification')
    invoke.add_argument('id', type=int, help='Notification ID')
    invoke.add_argument('action', nargs='?', help='Action to invoke. Default is "default"',
            default='default')

    sp.add_parser('clear', help='Clear all notifications')

    ns = parser.parse_args()

    subcommands = {'list': do_list, 'invoke': do_invoke, 'clear': do_clear, 'close':
            do_close}
    subcommands.get(ns.command, do_list)(ns)


def get_notifications_fname():
    default_notif_file_name = PTH.join('~', '.xmobar-notifications')
    if XDG_RUNTIME_DIR:
        notif_file_name = PTH.join(XDG_RUNTIME_DIR, 'xmobar-notifications')
    else:
        L.warning('"XDG_RUNTIME_DIR" environment variable not set. Falling back to'
                  f' {default_notif_file_name}')
        notif_file_name = default_notif_file_name

    return notif_file_name


def main():
    # Now we request the name and run the server.
    default_notif_pipe_file_name = PTH.join('~', '.xmobar-notifications-pipe')
    if XDG_RUNTIME_DIR:
        notif_pipe_file_name = PTH.join(XDG_RUNTIME_DIR, 'xmobar-notifications-pipe')
    else:
        L.warning('"XDG_RUNTIME_DIR" environment variable not set. Falling back to'
                  f' {default_notif_pipe_file_name}')
        notif_pipe_file_name = default_notif_pipe_file_name

    notif_file_name = get_notifications_fname()

    acq_handler = BusAcquisitionHandler(PTH.expanduser(PTH.expandvars(notif_file_name)),
                                        PTH.expanduser(PTH.expandvars(notif_pipe_file_name)))

    notifications_owner_id = Gio.bus_own_name(
        Gio.BusType.SESSION,  # What kinda bus is it?
        "org.freedesktop.Notifications",  # It's name ?
        Gio.BusNameOwnerFlags.NONE,  # If other has same name, what to do?
        acq_handler,
        on_name_acquired,
        on_name_lost)

    xmonoti_owner_id = Gio.bus_own_name(
        Gio.BusType.SESSION,  # What kinda bus is it?
        "cc.markw.Xmonoti",  # It's name ?
        Gio.BusNameOwnerFlags.NONE,  # If other has same name, what to do?
        acq_handler,
        on_name_acquired,
        on_name_lost)

    try:
        loop.run()  # Like a web server, ours has to be active for client access
    finally:
        Gio.bus_unown_name(notifications_owner_id)
        Gio.bus_unown_name(xmonoti_owner_id)
        acq_handler.close()
    print("Exiting...")


if __name__ == "__main__":
    main()

NOTIFICATIONS_XML = """\
<!DOCTYPE node PUBLIC '-//freedesktop//DTD D-BUS Object Introspection 1.0//EN'
'http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd'>
<node>
  <interface name="org.freedesktop.Notifications">

    <annotation name="org.gtk.GDBus.C.Name" value="FdNotifications" />

    <method name="CloseNotification">
      <arg type="u" name="id" direction="in" />
    </method>

    <method name="GetCapabilities">
      <arg type="as" name="capabilities" direction="out" />
    </method>

    <method name="GetServerInformation">
      <arg type="s" name="name" direction="out" />
      <arg type="s" name="vendor" direction="out" />
      <arg type="s" name="version" direction="out" />
      <arg type="s" name="spec_version" direction="out" />
    </method>

    <method name="Notify">
      <arg type="s" name="app_name" direction="in" />
      <arg type="u" name="replaces_id" direction="in" />
      <arg type="s" name="app_icon" direction="in" />
      <arg type="s" name="summary" direction="in" />
      <arg type="s" name="body" direction="in" />
      <arg type="as" name="actions" direction="in" />
      <arg type="a{sv}" name="hints" direction="in" />
      <arg type="i" name="expire_timeout" direction="in" />
      <arg type="u" name="id" direction="out" />
    </method>

    <signal name="ActionInvoked">
      <arg type="u" name="id" />
      <arg type="s" name="action_key" />
    </signal>

    <signal name="NotificationClosed">
      <arg type="u" name="id" />
      <arg type="u" name="reason" />
    </signal>

  </interface>

  <interface name="cc.markw.Xmonoti">

    <annotation name="org.gtk.GDBus.C.Name" value="Xmonoti" />

    <method name="Invoke">
      <arg type="u" name="id" direction="in" />
      <arg type="s" name="action" direction="in" />
    </method>
    <method name="Clear" />
  </interface>
</node>
"""
